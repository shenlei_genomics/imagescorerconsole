#ifndef UNIT_TESTER
#define UNIT_TESTER

#include "Utils.h"

class UnitTester
{
public:
	UnitTester();
	virtual ~UnitTester();

	/*test track cross and output*/
	void testTrackCrosses(const std::vector<std::string>& file_names,
		const bool is_extend,
		const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const float& pixel_per_dnb);

	/*test filter and affine to fine tune track cross*/
	void testFineTuneTrackCrossByBlock(const std::string& file_name,
		const bool is_extend,
		const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const float& pixel_per_dnb,
		const uint bk_id);

	/*test block DNBs arranged to be 2D array*/
	void testBlockDNBs2D(const std::string& file_name,
		const bool is_extend,
		const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const float& pixel_per_dnb,
		const uint bk_id);

	/*test get a quarter of block DNBs*/
	void testQuarterBlockDNBs(const std::string& file_name,
		const bool is_extend,
		const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const float& pixel_per_dnb,
		const uint bk_id,
		const int quarter_id);

	/*test get inside block ids by track cross id*/
	void testInBlkIDsByTCID();

	/*test get block ids by track cross id*/
	void testBlkIDsByTCID();

	/*test fill 100 blocks(Block2D)*/
	void testFillAllBlocks2D(const std::string& file_name,
		const bool is_extend,
		const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const float& pixel_per_dnb);

	/*test evaluate track cross accuracy*/
	void testEvaluateTrackCross(const std::vector<std::string>& file_names,
		const bool is_extend,
		const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const GoodPtParam& param,
		const float& pixel_per_dnb);
	void evaluateTrackCrossTest(cv::Mat& img,
		const std::vector<cv::Point2f>& tc_subpixel,
		const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const GoodPtParam& param,
		const float& pixel_per_dnb);

	/*test find good points*/
	void testFindGoodPoints(const std::vector<std::string>& file_names);

	///*test continous 2D dynamic array*/
	//void test2DContinousArray();

	///*test hough line*/
	//void testHoughTransform(const std::string& file_name,
	//	const bool is_extend,
	//	const std::vector<int>& grids_x,
	//	const std::vector<int>& grids_y,
	//	const float& pixel_per_dnb);

	void testTrackArearBackground(const std::string& file_name,
		const bool is_extend,
		const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const float& pixel_per_dnb);
};

#endif
