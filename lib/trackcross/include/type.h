#include <stdlib.h>
#include <stdio.h>
#if defined(_WIN32)
#include <io.h>
#endif
#include <iomanip>
#include <time.h>
#include <assert.h>
//#include <intrin.h>//all intrinsic # windows

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <list>
#include <queue>
#include <unordered_map>
#include <string>
#include <algorithm>
#include <numeric>

//#define PI					3.14159265358979323846  
//#define Deg2Rad(deg)		((deg) * PI / 180.0)
//#define Rad2Deg(rad)		((rad) * 180.0 / PI)

/* DNB_SIZE_IN_PIXEL : The pixel distance between two closest DNBs*/
#define PIXEL_PER_DNB	2.0 /*2.165*/ /*3.0*/

/*inner or all track crosses*/
#define ALL_BLOCK

//#define IMAGE_WIDTH	    1960
//#define IMAGE_HEIGHT		1960

//#define CHANNEL_NUMBER		2
#define NOISE_THRESHOLD		60000

//#define SHOW_TIME

/*whether to use fine tune*/
//#define USE_FINE_TUNE

//#define LOCATE_BUG

//#define PRINT_LOG

/*whether use integral image*/
#define USE_INTEGRAL

/*whether use standard template*/
//#define USE_STANDARD_TEMP

#define DISTORT 3.0f

typedef unsigned int uint;
