#ifndef CONFIG_PARSER
#define CONFIG_PARSER

#include <stdio.h>
#include <fstream>
#include <string>
#include <unordered_map>

typedef unsigned int uint;

class ConfigParser
{
public:
	ConfigParser();
	virtual ~ConfigParser();

	inline bool isSpace(char c)
	{
		if (' ' == c || '\t' == c)
			return true;
		return false;
	}

	bool isCommentChar(char c);

	void trim(std::string& str);

	bool parseLine(const std::string& line,
		std::string& key, std::string& value);

	int readConfig(const std::string& file_name,
		std::unordered_map<std::string, std::string>& m);

	inline void printConfig(const std::unordered_map<std::string, std::string>& m)
	{
		for (auto iter = m.begin(); iter != m.end(); ++iter)
			printf("%s = %s\n", iter->first, iter->second);
	}

	inline bool getParameter(const std::unordered_map<std::string, std::string>& m,
		const std::string& key, std::string& value)
	{
		auto iter = m.find(key);
		if (iter != m.end())//found
		{
			value = iter->second;
			return true;
		}
		else return false;
	}

private:
	static const char COMMENT_CHAR;
};

#endif
