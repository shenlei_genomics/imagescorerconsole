#ifndef CORRECTOR
#define CORRECTOR

#include "Utils.h"
#include "GravityTrackCrossLocator.h"

class Corrector
{
public:
	/*store infos of points need to be further corrected*/
	struct FurtherCorrectedPt
	{
		int pt_id, flag;
		cv::Point point;
	};

	/*track cross need to be further corrected*/
	struct FurtherCorrectedPtSubpixel
	{
		int pt_id, flag;
		cv::Point2f point;
	};

	/*comparator based on track cross score*/
	struct ScoreComparator
	{
		const bool operator() (const std::pair<int, double>& input_1,
		const std::pair<int, double>& input_2) const
		{
			return input_2.second < input_1.second;
		}
	};

	Corrector();
	Corrector(const std::pair<uint, uint>& track_num);
	Corrector(const std::pair<uint, uint>& track_num, const float& pixel_per_dnb);
	virtual ~Corrector();

	/*arrange points in rows and cols*/
	int arrangePts(const std::vector<cv::Point>& track_crosses);
	//according to track line number
	int arrangePts(const std::vector<cv::Point>& track_crosses, 
		const std::pair<uint, uint>& track_num);
	int arrangePts(const std::vector<cv::Point2f>& track_crosses);

	/*score points*/
	float getPointScore(const std::pair<int, cv::Point>& input,
		const int flag, const GravityTrackCrossLocator& locator);
	//according to track line number
	float getPointScore(const std::pair<int, cv::Point>& input,
		const int flag, const GravityTrackCrossLocator& locator, const std::pair<uint, uint>& track_num);
	float getPtMinScore(const std::pair<int, cv::Point>& input,
		const int flag,
		const GravityTrackCrossLocator& locator,
		const std::pair<uint, uint>& c_r);
	double getPointScore(const std::pair<int, cv::Point2f>& input,
		const int flag, const GravityTrackCrossLocator& locator);

	//use the median score
	double scoreTrackCross(const std::pair<int, cv::Point>& input, const int flag, const GravityTrackCrossLocator& locator);
	double scoreTrackCross(const std::pair<int, cv::Point2f>& input, const int flag, const GravityTrackCrossLocator& locator);
	//score track cross using median score without bad point skip
	double scoreTrackCrossWithoutBadSkip_1(const std::pair<int, cv::Point>& input, const GravityTrackCrossLocator& locator);
	double scoreTrackCrossWithoutBadSkip_2(const std::pair<int, cv::Point>& input, const GravityTrackCrossLocator& locator);

	/*score inner 49(7 x7) track crosses*/
	double scoreInnerTC(const std::pair<int, cv::Point>& input, const GravityTrackCrossLocator& locator, const std::vector<std::vector<cv::Point>>& blocks);
	inline double scoreInnerTC(const std::pair<int, cv::Point2f>& input, const GravityTrackCrossLocator& locator, const std::vector<std::vector<cv::Point2f>>& blocks)
	{
		return 0.5 * this->scoreTrackCrossWithoutBadSkip_1(input, locator)
			+ 0.5 * Utils::getTrackCrossDistortMin(input.first, blocks);
	}

	/*classify into good points and bad points*/
	int classsify(const GravityTrackCrossLocator& locator);
	int classify(const GravityTrackCrossLocator& locator, const std::vector<cv::Point>& track_crosses);
	//classify according to track line number
	int classify(const GravityTrackCrossLocator& locator, 
		const std::vector<cv::Point>& track_crosses, const std::pair<uint, uint>& track_num);
	
	int classify(const GravityTrackCrossLocator& locator,
		const std::vector<cv::Point>& track_crosses, const double threshold);

	// output track cross QC
	int classify(const GravityTrackCrossLocator& locator,
		const std::vector<cv::Point>& track_crosses,
		const std::pair<uint, uint>& track_num,
		TCOutput& out);

	//using block information
	int classify(const GravityTrackCrossLocator& locator, const std::vector<cv::Point>& track_crosses, const std::vector<std::vector<cv::Point>>& blocks);

	/*test classify and correct together*/
	int classifyInOut(const GravityTrackCrossLocator& locator, const std::vector<cv::Point>& track_crosses, const std::vector<std::vector<cv::Point>>& blocks);

	int classifySubpixel(const GravityTrackCrossLocator& locator, const std::vector<cv::Point2f>& track_crosses, const std::vector<std::vector<cv::Point2f>>& blocks);

	/*compound classifying method*/
	int classify(const GravityTrackCrossLocator & locator,
		const std::vector<std::pair<float, float>>& h_params,
		const std::vector<std::pair<float, float>>& v_params,
		const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const float& TH);

	/*using line segment to correct*/
	int pullPointToLine(const std::pair<cv::Point, cv::Point>& two_points, const int flag, cv::Point& pt_in);
	int pullPointToLine(const std::pair<cv::Point2f, cv::Point2f>& two_points, const int flag, cv::Point2f& pt_in);
	int correctByLine(std::vector<cv::Point>& track_crosses);

	/*get correction type by score*/
	int getCorrectFlagByScore(const std::pair<int, cv::Point2f>& input,
		const GravityTrackCrossLocator* locator,
		const std::vector<std::vector<cv::Point2f>>& rows_points,
		const std::vector<std::vector<cv::Point2f>>& cols_points,
		const std::vector<int>& bad_ids, const std::pair<uint, uint>& track_num,
		int& flag);

	/*using surrounding neighbors to correct*/
	int correctByNeighbor(std::vector<cv::Point>& track_crosses);
	//according to track line number
	int correctByNeighbor(const GravityTrackCrossLocator* locator,
		std::vector<cv::Point>& track_crosses, const std::pair<uint, uint>& track_num);

	/*score track cross*/
	float scoreTrackCross(const std::pair<int, cv::Point2f>& input,
		const GravityTrackCrossLocator* locator,
		const std::vector<std::vector<cv::Point2f>>& rows_points,
		const std::vector<std::vector<cv::Point2f>>& cols_points,
		const std::pair<uint, uint>& track_num);

	/*using local feature to correct*/
	int correctFurther(const cv::Mat& img, std::vector<cv::Point>& track_crosses);
	//int correctFurther(const cv::Mat& img,
	//	const GravityTrackCrossLocator* locator, std::vector<cv::Point>& track_crosses);
	//arbitrary template(arbitrary track number)
	int correctFurther(const cv::Mat& img, const GravityTrackCrossLocator* locator,
		const std::pair<uint, uint>& track_num, std::vector<cv::Point>& track_crosses);

	//int correctFurther(const cv::Mat& img, 
	//	const GravityTrackCrossLocator* locator, std::vector<cv::Point2f>& track_crosses);

	/*select 3 points of center 4 points*/
	int select3Of4CenterTC(const std::vector<cv::Point>& TCs, const GravityTrackCrossLocator& locator, std::vector<cv::Point>& out_3_points);
	//with tc_id return 
	int select3Of4CenterTC(const std::vector<cv::Point>& TCs, const GravityTrackCrossLocator& locator, std::vector<std::pair<int, cv::Point>>& out_3_points);
	int select3Of4CenterTC(const std::vector<cv::Point2f>& TCs, const GravityTrackCrossLocator& locator, std::vector<cv::Point2f>& out_3_points);
	int select3Of4CenterTC(const std::vector<cv::Point2f>& TCs, const GravityTrackCrossLocator& locator, std::vector<cv::Point2f>& out_3_points, std::vector<int>& center_tc_ids);
	int select3Of4CenterTC(const std::vector<cv::Point2f>& TCs, const GravityTrackCrossLocator& locator,
		const std::vector<std::vector<cv::Point2f>>& blocks, std::vector<cv::Point2f>& out_3_points, std::vector<int>& center_tc_ids);
	int select3Of4CenterTC(const std::vector<cv::Point2f>& TCs, const GravityTrackCrossLocator& locator,
		const std::vector<std::vector<cv::Point2f>>& blocks, const std::vector<int>& input_tc_ids, std::vector<cv::Point2f>& out_3_points, std::vector<int>& center_tc_ids);
	int select3Of4CenterTC(const std::vector<cv::Point2f>& TCs, const GravityTrackCrossLocator& locator,
		const std::vector<std::vector<cv::Point2f>>& blocks, const int(&input_tc_ids)[4], std::vector<cv::Point2f>& out_3_points, std::vector<int>& center_tc_ids);
	//select 3 points from 9 
	int select3Of9CenterTC(const std::vector<cv::Point2f>& TCs, const GravityTrackCrossLocator& locator,
		const std::vector<std::vector<cv::Point2f>>& blocks, std::vector<cv::Point2f>& out_3_points, std::vector<int>& center_tc_ids);
	//with tc_id return
	int select3Of4CenterTC(const std::vector<cv::Point2f>& points, const GravityTrackCrossLocator& locator, std::vector<std::pair<int, cv::Point2f>>& out_3_points);

	inline int setTCIDs(const int id_0, const int id_1, const int id_2, const int id_3)
	{
		this->m_TC_ids[0] = id_0;
		this->m_TC_ids[1] = id_1;
		this->m_TC_ids[2] = id_2;
		this->m_TC_ids[3] = id_3;
		return 0;
	}

protected:
	bool m_is_arranged;
	bool m_is_classified;
	std::vector<std::vector<cv::Point2f>> m_pts_y;
	std::vector<std::vector<cv::Point2f>> m_pts_x;
	std::vector<std::vector<cv::Point2f>> m_good_pts_y;
	std::vector<std::vector<cv::Point2f>> m_good_pts_x;

	/*store points to be corrected*/
	std::vector<int> m_bad_ids;
	std::vector<std::pair<int, cv::Point2f>> m_bad_pts;
	std::vector<std::pair<int, cv::Point2f>> m_bad_row_points[9];
	std::vector<std::pair<int, cv::Point2f>> m_bad_col_points[9];
	std::vector<FurtherCorrectedPt> m_filter_pts;
	std::vector<FurtherCorrectedPtSubpixel> m_filter_pts_subpixel;

	/*store 4 track cross ids for some operation*/
	int m_TC_ids[4];
	float m_threshold;

	friend class Utils;
	friend class CoordExtractor;
};

#endif
