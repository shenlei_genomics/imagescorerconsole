#ifndef MASK_PARAM
#define MASK_PARAM

#include "type.h"

class MaskParam
{
public:
	MaskParam(const uint mask_id);

	virtual ~MaskParam();

	// get parameters by mask ID
	void serMaskParam();

	// calcuate filed x and y coordinates
	bool calcFiledCoords();

	// initiate parameters
	void init();

// --------------------------------------------------members
	// mask ID
	uint m_id;

	// whether mask paramer is set
	bool m_is_set;

	// the actual dist(in ��m) per pixel
	float m_dist_per_pixel;

	// deletion pattern pitch (in ��m)
	float m_dele_pitch_x;
	float m_dele_pitch_y;

	// deletion pattern pitch (in pixel)
	float m_dele_pitch_x_px;
	float m_dele_pitch_y_px;

	// mask width and height(in ��m)
	float m_mask_width;
	float m_mask_height;

	// image width and height(in pixel)
	uint m_img_width;
	uint m_img_height;

	// x and y coordinates(track line area center in ��m)
	std::vector<float> m_mask_x;
	std::vector<float> m_mask_y;

	//x and y coordinates(track line area center in pixel)
	std::vector<float> m_filed_x;
	std::vector<float> m_filed_y;

	// deletion pattern size(how many kinds of DNB in the track area)
	uint m_dele_size;

	// deletion pattern's bits number
	uint m_bits_num;
};

#endif
