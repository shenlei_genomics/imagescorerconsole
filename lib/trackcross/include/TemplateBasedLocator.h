#ifndef TEMPLATE_BASED_LOCATOR
#define TEMPLATE_BASED_LOCATOR

#include "TrackCrossLocator.h"

class TemplateBasedLocator : public TrackCrossLocator
{
public:
	TemplateBasedLocator();
	virtual int locateTrackCrossInPixel(const std::vector<int>& grids_x,
		const std::vector<int>& grids_y,
		const float pixel_per_dnb,
		std::vector<cv::Point>& pts_pixel);
	virtual ~TemplateBasedLocator();
};

#endif
