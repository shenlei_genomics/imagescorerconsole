#ifndef _FOV_BLOCK_H_
#define _FOV_BLOCK_H_

#include "Utils.h"

class FOVBlock
{
public:
	FOVBlock();
	virtual ~FOVBlock();
	int getBlockDNBPoints();

	//protected:
	int m_block_id;
	int m_block_dnb_numX;
	int m_block_dnb_numY;
	std::vector<cv::Point2f> m_vertices;  // 4 vertex , modified by LM, 10.14.2016
	std::vector<cv::Point2f> m_dnb_pts;

	//friend class Utils;
	//friend class TrackCrossLocator;
	//friend class IntensitiesExtractionRTA;
};

int getDNBPoints(const int mode, const std::vector<cv::Point>& grid_sizes, const std::vector<FOVBlock>& blocks, std::vector<cv::Point2f>& dnb_points);

#endif
