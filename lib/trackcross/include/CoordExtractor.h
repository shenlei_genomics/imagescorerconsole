#ifndef COORD_EXTRACTOR
#define COORD_EXTRACTOR

#include "Utils.h"
#include "CoordExtractor.h"
#include "GravityTrackCrossLocator.h"

class CoordExtractor
{
public:
	CoordExtractor(const cv::Mat& img, const int cycle,
		const int channel, const int fov_row, const int fov_col,
		const std::vector<int>& dnb_numsX, const std::vector<int>& dnb_numsY);
	CoordExtractor(const cv::Mat& img,
		const int channel, const int fov_row, const int fov_col,
		const std::vector<int>& dnb_numsX, const std::vector<int>& dnb_numsY);
	CoordExtractor(const cv::Mat& img,
		const std::vector<int>& dnb_numsX, const std::vector<int>& dnb_numsY);
	CoordExtractor(const std::vector<int>& dnb_numsX,
		const std::vector<int>& dnb_numsY);	
	CoordExtractor();
	virtual ~CoordExtractor();

	int init(const cv::Mat& img,
		const std::vector<int>& dnb_numsX, const std::vector<int>& dnb_numsY);

	/*set block DNB numbers*/
	int setBlockDNBNum(const std::vector<int>& dnb_numsX,
		const std::vector<int>& dnb_numsY);

	/*
	* set processing image
	* delegate mode here
	*/
	inline void setImage(cv::Mat& img)
	{
		m_tc_locator->setImage(img);
	}

	/*get processed image*/
	inline cv::Mat& getImage()
	{
		return this->m_tc_locator->getImage();
	}

	/*get integral image*/
	inline cv::Mat& getInteg()
	{
		return this->m_tc_locator->getInteg();
	}

	/*init integral image*/
	inline void init()
	{
#ifdef USE_INTEGRAL
		m_tc_locator->initIntegral();
#else
		//if (m_tc_locator->m_img.type() != CV_32F)
		m_tc_locator->m_img.convertTo(m_tc_locator->m_img, CV_32F);
#endif
	}

	/*get track cross extractor*/
	inline GravityTrackCrossLocator* getTrackCrossLocator()
	{
		return this->m_tc_locator;
	}

	/*set track cross locator*/
	inline int setTrackCrossLocator(const GravityTrackCrossLocator* locator)
	{
		if (!locator)
		{
			printf("[Error]: input point's NULL.\n");
			return -1;
		}

		this->m_tc_locator = const_cast<GravityTrackCrossLocator*>(locator);
		return 0;
	}

	int locateAndCorrect(TCOutput& out,
		const float& pixel_per_dnb);

	// overload with dynamic scan param
	int locateAndCorrect(TCOutput& out,
		const float& pixel_per_dnb, ScanParam& param);

	inline int extractTrackCross(const float& pixel_per_dnb,
		ScanParam& param,
		const bool is_entend,
		TCOutput& out)
	{
		this->init(); // init image type and integral image
		int ret = this->locateAndCorrect(out, pixel_per_dnb, param);
		if (ret < 0)
			return ret;
		m_tc_locator->locateTrackCrossSubpixel(out.m_tracks_pixel, pixel_per_dnb);
		if (is_entend)
		{
			if (out.m_track_ids_x.size() == 9 && out.m_track_ids_y.size() == 9
				&& out.m_track_ids_y[0] == 1 && out.m_track_ids_x[0] == 1)
			{
				ret = m_tc_locator->extendTrackCross(m_grids_x, m_grids_y); // standard template
			}
			else // arbitrary template
				ret = m_tc_locator->extendTrackCross(pixel_per_dnb, m_grids_x, m_grids_y);
			if (ret < 0)
				return ret;
		}
		out.m_tracks_subpixel = this->getTrackCrosses();
		return 0;
	}

	inline int extractTrackCross(const float& pixel_per_dnb,
		const bool is_entend,
		TCOutput& out)
	{
#ifdef SHOW_TIME
		clock_t time_0 = clock();
#endif

		/************************************************************/
		this->init();
		int ret = this->locateAndCorrect(out, pixel_per_dnb);
		if (ret < 0) return ret;
		m_tc_locator->locateTrackCrossSubpixel(out.m_tracks_pixel, pixel_per_dnb);
		if (is_entend)
		{
			if (out.m_track_ids_x.size() == 9 && out.m_track_ids_y.size() == 9
				&& out.m_track_ids_y[0] == 1 && out.m_track_ids_x[0] == 1)
			{
				ret = m_tc_locator->extendTrackCross(m_grids_x, m_grids_y);//standard template
			}
			else//arbitrary template
				ret = m_tc_locator->extendTrackCross(pixel_per_dnb, m_grids_x, m_grids_y);
			if (ret < 0) return ret;
		}
		out.m_tracks_subpixel = this->getTrackCrosses();
#ifdef USE_FINE_TUNE//debug here...
		if (!m_is_block_meshed) this->meshBlocks();
		GoodPtParam param;
		if (pixel_per_dnb < 3.0f) param.init(3.0f, 0.45f, 0.068f);//V0.1 parameter
		else param.init(2.45f, 0.29f, 0.12f);//V1 parameter
		Utils::fillBlockVertexDNB(out.m_tracks_subpixel, m_blocks);
		Utils::fineTune(m_tc_locator->m_img, m_blocks, param, pixel_per_dnb, out);
#endif
		/************************************************************/

#ifdef SHOW_TIME
		clock_t time_1 = clock();
		printf("<detect> run time: %.3fms\n\n",
			float(time_1 - time_0) / CLOCKS_PER_SEC * 1000.0f);
#endif

		return 0;
	}
	
			
	//use first cycle inner 81 track crosses to affine
	inline int calcTrackCross(const float& pixel_per_dnb, std::vector<cv::Point2f>& in_TCs)
	{

		return 0;
	}

	float scoreTrackCross(const std::pair<int, cv::Point>& input,
		const std::vector<std::vector<cv::Point>>& rows_points, const std::vector<std::vector<cv::Point>>& cols_points);
	double scoreTC(const std::pair<int, cv::Point>& input,
		const std::vector<std::vector<cv::Point>>& rows_points, const std::vector<std::vector<cv::Point>>& cols_points);
	double scoreTrackCrosses(const std::vector<cv::Point>& coarse_points);
	double scoreTCs(const std::vector<cv::Point>& coarse_points);

	//int extractDNBPoints(const int mode);

	/*get DNB points of given block*/
	int getBlockDNBPoints(const int block_id, std::vector<cv::Point2f>& out_pts);

	/*get and set track crosses*/
	inline const std::vector<cv::Point2f>& getTrackCrosses()
	{
		return this->m_tc_locator->m_tc_pts;
	}
	inline const std::vector<cv::Point2f>& getInnerTrackCrosses()
	{
		return this->m_tc_locator->m_inner_Track_cross_pts;
	}
	inline void setTrackCrosses(const std::vector<cv::Point2f>& track_crosses)
	{
		this->m_tc_locator->m_tc_pts = track_crosses;
	}
	inline void copyTrackCrosses(const std::vector<cv::Point2f>& track_crosses)
	{
		this->m_tc_locator->m_tc_pts.resize(track_crosses.size(), cv::Point2f(0, 0));
		std::copy(track_crosses.begin(), track_crosses.end(), this->m_tc_locator->m_tc_pts.begin());
	}

	/*mesh blocks*/
	inline void meshBlocks()
	{
		Utils::getDNBVector(m_grids_x, m_grids_y, m_dnb_vect_x, m_dnb_vect_y);
		uint x, y;
		for (uint i = 0; i < 100; ++i)
		{
			x = i % 10;
			y = i / 10;
			m_blocks[i].m_id = i;
			m_blocks[i].m_grid.first = m_dnb_vect_x[x];
			m_blocks[i].m_grid.second = m_dnb_vect_y[y];
		}
		m_is_block_meshed = true;
	}

protected:
	std::vector<int> m_grids_x;//template block X DNB number
	std::vector<int> m_grids_y;//template block Y DNB number

	std::vector<int> m_dnb_vect_x;
	std::vector<int> m_dnb_vect_y;

	GravityTrackCrossLocator* m_tc_locator;//track_cross_locator:can be replaced by other sub classes
	GravityTrackCrossLocator m_locator;
	std::vector<cv::Point2f> m_dnb_points;//store all DNB points' coordinates

	/*used for testing affine track cross calculation*/
	std::vector<int> m_template_center_tc_ids;
	std::vector<cv::Point2f> m_templateTCs, m_template_sample_TCs;

	/*mesh blocks*/
	bool m_is_block_meshed;
	Block m_blocks[100];

	friend class FOV;
	friend class Utils;
	friend class UnitTester;
};

#endif
