#ifndef LINE_FITTER
#define LINE_FITTER

#include "Utils.h"

class LineFitter
{
public:
	LineFitter();
	~LineFitter();

	/*least square with analytic method*/
	int fitANA(const float*& x_vector, const float*& y_vector, const int element_num, cv::Point2f& pt_params);
	int fitANA(const std::vector<cv::Point2f>& pts_in, cv::Point2f& pt_params);
	int fitANA(const std::vector<cv::Point2f>& pts_in, std::pair<double, double>& line_params);

	/*use OpenCV line fitting*/
	inline int fitCV(const std::vector<cv::Point2f>& pts_in, std::pair<float, float>& line_params)
	{
		if (pts_in.size() < 2)
		{
			printf("[Error]: input point vector is too few.\n");			
			return -1;
		}
		float k;
		cv::Vec4f line;
		cv::fitLine(cv::Mat(pts_in), line, CV_DIST_HUBER, 0, 0.01, 0.01);
		if (line[0])//2D�� line[0--3]:vx, vy, x0, y0
		{
			k = line[1] / line[0];
			line_params.first = k;//k
		}
		line_params.second = line[3] - k * line[2];//b
		return 0;
	}
	inline int fitCV(const std::vector<cv::Point2f>& pts_in, float& k, float& b)
	{
		if (pts_in.size() < 2)
		{
			printf("[Error]: input point vector is too few.\n");
			return -1;
		}
		cv::Vec4f line;
		cv::fitLine(cv::Mat(pts_in), line, CV_DIST_HUBER, 0, 0.01, 0.01);
		if (line[0])//2D�� line[0--3]:vx, vy, x0, y0
		{
			k = line[1] / line[0];
		}
		b = line[3] - k * line[2];//b
		return 0;
	}

	int fitCV(const std::vector<cv::Point2f>& pts_in, std::vector<double>& line_params);

	float getLinearity(CvSeq *seq);
};

#endif
