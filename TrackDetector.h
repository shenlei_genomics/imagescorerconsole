#ifndef TRACK_DETECTOR
#define TRACK_DETECTOR

#include "CoordExtractor.h"

class TrackDetector
{
public:
	// constructor 
	TrackDetector(const std::vector<int>& grids_x,
		const std::vector<int>& grids_y, 
		const float& pixel_per_dnb);

	// deconstructor
	virtual ~TrackDetector();

	// run function
	inline int detectTracks(cv::Mat& img,
		TCOutput& output,
		const bool is_extend,
		const float& scan_ratio)
	{// use a segment of 200 pixels as feature
		if (!m_is_init) // initiate scan area and feature length
		{
			int radius = MIN(cvRound((float)img.cols * scan_ratio),
				cvRound((float)img.rows * scan_ratio));
			const int MID_X = (img.cols >> 1);
			const int MID_Y = (img.rows >> 1);
			m_scan_param.m_x_begin = MID_X - radius; // specify a certain area
			m_scan_param.m_y_begin = MID_Y - radius;
			m_scan_param.m_x_end = MID_X + radius;
			m_scan_param.m_y_end = MID_Y + radius;
			if (m_scan_param.m_x_end + m_allowance.first >= img.cols)
			{
				m_scan_param.m_x_end = img.cols - m_allowance.first;
				m_scan_param.m_x_begin = m_scan_param.m_x_end - (radius << 1);
			}
			if (m_scan_param.m_y_end + m_allowance.second >= img.rows)
			{
				m_scan_param.m_y_end = img.rows - m_allowance.second;
				m_scan_param.m_y_begin = m_scan_param.m_y_end - (radius << 1);
			}
			while (m_scan_param.m_x_begin < 0
				|| m_scan_param.m_y_begin < 0)
			{
				radius = cvRound(radius * 0.85f); // iterate to the most proper radius
				m_scan_param.m_x_begin = MID_X - radius; // specify a certain area
				m_scan_param.m_y_begin = MID_Y - radius;
				m_scan_param.m_x_end = MID_X + radius;
				m_scan_param.m_y_end = MID_Y + radius;
				if (m_scan_param.m_x_end + m_allowance.first >= img.cols)
				{
					m_scan_param.m_x_end = img.cols - m_allowance.first;
					m_scan_param.m_x_begin = m_scan_param.m_x_end - (radius << 1);
				}
				if (m_scan_param.m_y_end + m_allowance.second >= img.rows)
				{
					m_scan_param.m_y_end = img.rows - m_allowance.second;
					m_scan_param.m_y_begin = m_scan_param.m_y_end - (radius << 1);
				}
			}

			printf("--final radius: %d\n", radius);

			m_scan_param.m_feat_left = ((m_scan_param.m_x_begin + m_scan_param.m_x_end) >> 1) - 100;
			m_scan_param.m_feat_right = ((m_scan_param.m_x_begin + m_scan_param.m_x_end) >> 1) + 100;
			m_scan_param.m_feat_up = ((m_scan_param.m_y_begin + m_scan_param.m_y_end) >> 1) - 100;
			m_scan_param.m_feat_down = ((m_scan_param.m_y_begin + m_scan_param.m_y_end) >> 1) + 100;
			m_scan_param.m_allowance = this->m_allowance; // store allowance into scan parameter struct
			m_is_init = true;
		}
		m_extractor->setImage(img);
		int ret = m_extractor->extractTrackCross(m_pixel_per_dnb,
			m_scan_param, is_extend, output);
		if (ret < 0)
			return ret;
		return 0;
	}

	inline int detectTracks(cv::Mat& img, 
		TCOutput& output,
		const bool is_extend,
		int& radius)
	{// use a segment of 200 pixels as feature
		if (!m_is_init) // initiate scan area and feature length
		{
			const int MID_X = (img.cols >> 1);
			const int MID_Y = (img.rows >> 1);
			m_scan_param.m_x_begin = MID_X - radius; // specify a certain area
			m_scan_param.m_y_begin = MID_Y - radius;
			m_scan_param.m_x_end = MID_X + radius;
			m_scan_param.m_y_end = MID_Y + radius;
			if (m_scan_param.m_x_end + m_allowance.first >= img.cols)
			{
				m_scan_param.m_x_end = img.cols - m_allowance.first;
				m_scan_param.m_x_begin = m_scan_param.m_x_end - (radius << 1);
			}
			if (m_scan_param.m_y_end + m_allowance.second >= img.rows)
			{
				m_scan_param.m_y_end = img.rows - m_allowance.second;
				m_scan_param.m_y_begin = m_scan_param.m_y_end - (radius << 1);
			}
			while (m_scan_param.m_x_begin < 0
				|| m_scan_param.m_y_begin < 0)
			{
				radius = cvRound(radius * 0.85f); // iterate to the most proper radius
				m_scan_param.m_x_begin = MID_X - radius; // specify a certain area
				m_scan_param.m_y_begin = MID_Y - radius;
				m_scan_param.m_x_end = MID_X + radius;
				m_scan_param.m_y_end = MID_Y + radius;
				if (m_scan_param.m_x_end + m_allowance.first >= img.cols)
				{
					m_scan_param.m_x_end = img.cols - m_allowance.first;
					m_scan_param.m_x_begin = m_scan_param.m_x_end - (radius << 1);
				}
				if (m_scan_param.m_y_end + m_allowance.second >= img.rows)
				{
					m_scan_param.m_y_end = img.rows - m_allowance.second;
					m_scan_param.m_y_begin = m_scan_param.m_y_end - (radius << 1);
				}
			}

			//printf("--final radius: %d\n", radius);

			m_scan_param.m_feat_left = ((m_scan_param.m_x_begin + m_scan_param.m_x_end) >> 1) - 100;
			m_scan_param.m_feat_right = ((m_scan_param.m_x_begin + m_scan_param.m_x_end) >> 1) + 100;
			m_scan_param.m_feat_up = ((m_scan_param.m_y_begin + m_scan_param.m_y_end) >> 1) - 100;
			m_scan_param.m_feat_down = ((m_scan_param.m_y_begin + m_scan_param.m_y_end) >> 1) + 100;
			m_scan_param.m_allowance = this->m_allowance; // store allowance into scan parameter struct
			m_is_init = true;
		}
		m_extractor->setImage(img);
		int ret = m_extractor->extractTrackCross(m_pixel_per_dnb, 
			m_scan_param, is_extend, output);
		if (ret < 0) 
			return ret;
		return 0;
	}

	/*get processed image*/
	inline cv::Mat& getImage()
	{
		return this->m_extractor->getImage();
	}

	/*get processed image*/
	inline cv::Mat& getInteg()
	{
		return this->m_extractor->getInteg();
	}

	/*intiate allowance*/
	inline void initAllowance(const std::vector<int>& grids_x,
		const std::vector<int>& grids_y, const float& pixel_per_dnb)
	{ // find the max two consective track line distance, 100: correct allowance
		const auto x_max = std::max_element(grids_x.begin(), grids_x.end());
		int x_grid_id = (int)std::distance(grids_x.begin(), x_max);
		if (++x_grid_id > 8) x_grid_id -= 9; // loop back
		const auto x_sec_max = grids_x.begin() + x_grid_id;
		const auto y_max = std::max_element(grids_y.begin(), grids_y.end());
		int y_grid_id = (int)std::distance(grids_y.begin(), y_max);
		if (++y_grid_id > 8) y_grid_id -= 9;
		const auto y_sec_max = grids_y.begin() + y_grid_id;
		m_allowance.first = int((*x_max + *x_sec_max) * pixel_per_dnb + 0.5f + 100.0f);
		m_allowance.second = int((*y_max + *y_sec_max) * pixel_per_dnb + 0.5f + 100.0f);
	}

private:
	float m_pixel_per_dnb;
	CoordExtractor* m_extractor;

	std::pair<int, int> m_allowance; // template allowance
	ScanParam m_scan_param;
	bool m_is_init;

	friend class UnitTester;
};

/*check Nan or Inf*/
//const uint SIZE = (uint)output.m_tracks_subpixel.size();
//for (uint i = 0; i < SIZE; ++i)
//{
//	if (isnan(output.m_tracks_subpixel[i].x) || isnan(output.m_tracks_subpixel[i].y))
//	{
//		printf("[Error]: track cross is Nan or Inf.\n");
//		//CGI_WARN(CGI::OSIIP::gclog, "[Error]: track cross is Nan or Inf.\n");
//		return -1;
//	}
//}

#endif
