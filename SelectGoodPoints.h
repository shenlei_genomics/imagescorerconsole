#pragma once

#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

//if intensity > INTENS_TH , high probability of mass of noise, filter this point
const float INTENS_TH = 40000.0f;
struct PARAM
{
	//default threshold
	/*float intensPercentTh = 0.5f;
	float focusScorePercentTh = 0.8f;
	float centroidLowTh = -0.15f;
	float centroidUpTh = 0.15f;*/
	
	float intensPercentTh;
	float focusScorePercentTh;	
	float centroidLowTh;
	float centroidUpTh;
	PARAM():intensPercentTh(0.6f), focusScorePercentTh(0.8f), centroidLowTh(-0.1f), centroidUpTh(0.1f) {};
	PARAM(const float i,const float f, const float cl, const float cu):intensPercentTh(i), focusScorePercentTh(f), centroidLowTh(cl), centroidUpTh(cu) {};
};

float getThreshold(std::vector<float>& v, const float& thPercent);

inline static bool judgePeakPoint(const cv::Mat& img, const cv::Point& pt_in)
{
	if (img.at<float>(pt_in.y, pt_in.x) > INTENS_TH 
		|| img.at<float>(pt_in.y, pt_in.x) <= img.at<float>(pt_in.y - 1, pt_in.x - 1)
		|| img.at<float>(pt_in.y, pt_in.x) <= img.at<float>(pt_in.y - 1, pt_in.x)
		|| img.at<float>(pt_in.y, pt_in.x) <= img.at<float>(pt_in.y - 1, pt_in.x + 1)
		|| img.at<float>(pt_in.y, pt_in.x) <= img.at<float>(pt_in.y, pt_in.x - 1)
		|| img.at<float>(pt_in.y, pt_in.x) <= img.at<float>(pt_in.y, pt_in.x + 1)
		|| img.at<float>(pt_in.y, pt_in.x) <= img.at<float>(pt_in.y + 1, pt_in.x - 1)
		|| img.at<float>(pt_in.y, pt_in.x) <= img.at<float>(pt_in.y + 1, pt_in.x)
		|| img.at<float>(pt_in.y, pt_in.x) <= img.at<float>(pt_in.y + 1, pt_in.x + 1))
	{
		return false;
	}
	else
	{
		//std::cout << pt_in << std::endl;
		return true;
	}

	
}

//float getThreshold(vector<float>& v, const float& thPercent);

struct PeakInfo
{
	cv::Point loc;
	float focus;
	float intens;
	float cen_x;
	float cen_y;
	PeakInfo() :loc(cv::Point(0, 0)), focus(0.0f), intens(0.0f), cen_x(0.0f), cen_y(0.0f) {};
	PeakInfo(const cv::Point& p,float f, float i,float cx, float cy) :loc(p), focus(f), intens(i), cen_x(cx), cen_y(cy) {};
	~PeakInfo() {};
};

//inline void findPeakPtsVals(const cv::Mat& img, vector<PeakInfo>& peak, const PARAM& param = PARAM())
void findPeakPtsVals(const cv::Mat& img, std::vector<PeakInfo>& peak, const PARAM& param = PARAM());

int findGoodPoints(const std::string& imgFn, const std::string& gpMaskFn, const PARAM& param = PARAM());

int findGoodPoints(const std::string& imgFn, cv::Mat& goodPointsMask, cv::Mat(&centroidMat)[2], const PARAM& param = PARAM());
