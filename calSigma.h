#pragma once
#include "SelectGoodPoints.h"
#include <iterator>
#include <iostream>
#include <io.h>
#include "imgScoreType.h"

struct SigmaInfo
{
	cv::Point loc;
	float sigma_x;
	float sigma_y;
	float R2;
	SigmaInfo() :loc(cv::Point(0, 0)), sigma_x(0.0f), sigma_y(0.0f), R2(0.0f) {};
	SigmaInfo(const float sx, const float sy, const float r) :sigma_x(sx), sigma_y(sy), R2(r) {};
	SigmaInfo(const cv::Point& l, float sx, const float sy, const float r) :loc(l),sigma_x(sx), sigma_y(sy), R2(r) {};
};

int calSingleDNBSigma(const cv::Mat& img, const cv::Point& loc, float& sigma_x, float& sigma_y, float& R2, const unsigned int radius=1);

int calImgSigma(const cv::Mat& img, const std::vector<PeakInfo>& peak, std::vector<SigmaInfo>& sigma_vec, const float R2th_percent = 0.5);

//int getFocusScoreAndSigma(const char* img_fn, float& avg_focus_score, float& avg_sigma_x, float& avg_sigma_y, float& avg_R2, const bool ifsave_goodpoints = true);

extern "C" __declspec(dllexport) int getFocusScoreAndSigma(const char* img_fn, float& avg_focus_score, float& avg_sigma_x, float& avg_sigma_y, float& avg_R2, const bool ifsave_goodpoints);
int getFocusScoreAndSigma(const cv::Mat& img, float& avg_sigma_x, float& avg_sigma_y, float& avg_R2);

