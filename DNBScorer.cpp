#include "DNBScorer.h"
#include <io.h>
#include <map>
#include "GetConfig.h"
#include <iostream>
#include "SelectGoodPoints.h"


DNBScorer::~DNBScorer()
{
	this->dnb_numsX_tcdet.clear();
	this->dnb_numsY_tcdet.clear();
	this->DNB_NUM_X.clear();
	this->DNB_NUM_Y.clear();	
}

int DNBScorer::init(const string & config_fn)
{
	if (_access(config_fn.c_str(), 0) < 0)
	{
		return FILE_NOT_EXIST;
	}
	
	std::map<std::string, std::string> config_map;
	if (!ReadConfig(config_fn, config_map))
	{
		std::cerr << "Read config file:" << config_fn << " error!" << std::endl;
		return READ_CONFIG_ERROR;
	}
	string strChippitch, strImgscale, strDnbnumX, strDnbnumY, strdividedRadius = "0.6" , strIsExtend;
	bool ret = getParameter(config_map, "modparam.basecall.ImageTrackPattern_gridSizeX[]", strDnbnumX) && \
		getParameter(config_map, "modparam.basecall.ImageTrackPattern_gridSizeY[]", strDnbnumY) && \
		getParameter(config_map, "modparam.basecall.chip_pitch", strChippitch) && \
		getParameter(config_map, "modparam.basecall.img_scale", strImgscale) && \
		getParameter(config_map, "modparam.basecall.trackExtend", strIsExtend);
	if (!ret)
	{
		return READ_CONFIG_ERROR;
	}
	if (!getParameter(config_map, "modparam.basecall.pixel_radius", strdividedRadius))
	{
		dividedRadius = atof(strChippitch.c_str()) / (2 * atof(strImgscale.c_str()));
	}
	else
	{
		dividedRadius = atof(strdividedRadius.c_str());
	}
	if (BlockInt::initPitchsize(strChippitch, strImgscale) < 0)
	{
		return INIT_PITCH_SIZE_ERROR;
	}	
	this->is_extend = (strIsExtend == "true" ? true : false);
	parseStr(strDnbnumX, this->dnb_numsX_tcdet);
	parseStr(strDnbnumY, this->dnb_numsY_tcdet);
	parseVec(this->dnb_numsX_tcdet, this->is_extend, this->DNB_NUM_X);
	parseVec(this->dnb_numsY_tcdet, this->is_extend, this->DNB_NUM_Y);	
	this->BLOCKROW = DNB_NUM_Y.size();
	this->BLOCKCOL = DNB_NUM_X.size();
	ret = generateRoundSumFactorTable(this->unitNum, this->dividedRadius);
	return ret;
}


int DNBScorer::initBlockInts(const cv::Mat& img, const vector<Point2f> &trackcross, vector<BlockInt> &blocks)
{
	for (size_t row = 0; row < this->BLOCKROW; row++)
	{
		for (size_t col = 0; col < this->BLOCKCOL; col++)
		{
			size_t block_id = row * this->BLOCKCOL + col;
			vector<Point2f> vers(4);
			size_t topleftVerIndex = row * (this->BLOCKCOL + 1) + col;
			//P0-P4  clockwise
			vers[0] = trackcross[topleftVerIndex]; vers[1] = trackcross[topleftVerIndex + 1];
			vers[2] = trackcross[topleftVerIndex + 1 + this->BLOCKCOL + 1]; vers[3] = trackcross[topleftVerIndex + this->BLOCKCOL + 1];
			assert(this->DNB_NUM_X.size() > col && this->DNB_NUM_Y.size() > row);
			blocks.emplace_back(this->DNB_NUM_X[col], this->DNB_NUM_Y[row], vers, block_id);
			if (blocks[row*this->BLOCKCOL + col].initBlock(img) < 0)
			{
				std::cerr << "Error:calculate block info wrong! Block id:" << row* this->BLOCKCOL + col <<"\tIn function:"<<__FUNCTION__ <<"\tLine:"<<__LINE__<< std::endl;
				return INIT_BLOCK_ERROR;
			}
		}
	}
	return 0;
}

int DNBScorer::extractBlocksDnbs(const cv::Mat& img, vector<BlockInt>& blocks, std::vector<BlockDNBInfo>& blocksdnbinfo)
{	
	TrackDetector detector(this->dnb_numsX_tcdet, this->dnb_numsY_tcdet, BlockInt::pitchSize);
	TCOutput trackcross;
	//calculate the extended blocks, but will record is_extend and consider this in next process	
	if (detector.detectTracks(const_cast<cv::Mat&>(img), trackcross, is_extend, 0.27f) < 0)
	{
		return FIND_TC_FAILED;
	}
	blocks.clear();	
	initBlockInts(img, trackcross.m_tracks_subpixel, blocks);
#ifdef DEBUG
	std::ofstream  fs("./tc.txt");
	std::copy(trackcross.m_tracks_subpixel.begin(), trackcross.m_tracks_subpixel.end(), std::ostream_iterator<Point2f>(fs, "\n"));
	fs.close();
#endif // DEBUG	
	blocksdnbinfo.clear();
	for (size_t block_id = 0; block_id < BLOCKROW*BLOCKCOL; block_id++)
	{
		BlockDNBInfo blkdnb;
		intensityExtractionByBlock(img, blocks[block_id], this->unitNum, blkdnb);
		blocksdnbinfo.push_back(blkdnb);
	}
	return 0;
}


void DNBScorer::fillVerts(const vector<float>& verbk, const size_t& row, const size_t& col, vector<vector<bool>>& tc_if_traverse, vector<float>& tcbk_vec)
{
	assert(verbk.size() == 4);
	for (size_t i = 0; i < 4; i++)
	{
		size_t col_offset = i % 3 > 0 ? 1 : 0;
		if (tc_if_traverse[row + i / 2][col + col_offset] == false)
		{
			tcbk_vec.push_back(verbk[i]);
			tc_if_traverse[row + i / 2][col + col_offset] = true;
		}
	}
}

// get background std
int DNBScorer::getBkStd(const std::vector<BlockInt>& blocks, float& bk_std)
{
	assert(this->DNB_NUM_Y.size() > 2 && this->DNB_NUM_X.size() > 2);
	//init if_traverse_flag for each trackcross in INSIDE block.
	vector<vector<bool>> tc_if_traverse;
	for (size_t i = 0; i < this->DNB_NUM_Y.size() + 1; i++)
	{
		vector<bool> if_traverse_per_row(this->DNB_NUM_X.size() + 1, false);
		tc_if_traverse.push_back(if_traverse_per_row);
	}	
	vector<float> tcbk;
	for (size_t i = 0; i < this->BLOCKROW; i++)
	{
		for (size_t j = 0; j < this->BLOCKCOL; j++)
		{
			size_t block_id = i*this->BLOCKCOL + j;
			fillVerts(blocks[block_id].tcBkInts, i, j, tc_if_traverse, tcbk);
		}
	}
	if (tcbk.size() < 2)
	{
		return SIZE_ERROR;
	}
	float sum = std::accumulate(std::begin(tcbk), std::end(tcbk), 0.0f);
	float mean = sum / tcbk.size();
	float accum = 0.0f;
	std::for_each(std::begin(tcbk), std::end(tcbk), [mean, &accum](const float d) {accum += (d - mean)*(d - mean); });
	bk_std = sqrt(accum / (tcbk.size() - 1));
	return 0;
}

/** @brief input all DNBs intensity, calculate the img's historgram, find the first trough as the binary threshold.

@param intens      img's dnb's intensity set.
@param thresh      output binary threshold.
*/
int DNBScorer::getBinaryThresh(const std::vector<float>& intens, float& thresh)
{	
	using std::ofstream;
	const int step = 5;
	const int bins = 65535 / step;
	float hist[bins + 1] = { 0.0f };
	int index = 0;
#ifdef DEBUG
	ofstream fs("./intensv1.txt", std::ios::app);
#endif
	for (vector<float>::const_iterator it = intens.cbegin(); it != intens.cend(); ++it)
	{
		if (*it > 0)
		{
			index = *it / step;
			index = index > bins ? bins : index;
			hist[index] += 1;
		}
	}
#ifdef DEBUG
	std::copy(hist, hist + bins + 1, std::ostream_iterator<float>(fs, "\t"));
	fs << "\n";
#endif	
	const int k_size = 5, kr = k_size / 2;
	const size_t right_cut_band = 30000 / step; // cut the highest part of histogram to avoid influence of bright spot
	size_t start = kr, end = bins - k_size + 1 - right_cut_band;
	float part = 1.0f / k_size;
	const float kernel[k_size] = { 0.15,0.2,0.3,0.2,0.15 };//{0.3,0.4,0.3};
														   //std::cout << kernel[0] <<"\t"<< kernel[1] << std::endl;
	float smoothed_hist[bins + 1] = { 0.0f };
	for (size_t i = start; i < end + 1; i++)
	{
		for (size_t j = 0; j < k_size; j++)
		{
			smoothed_hist[i] += hist[i - kr + j] * kernel[j];
		}
	}
#ifdef DEBUG
	std::copy(smoothed_hist, smoothed_hist + bins + 1, std::ostream_iterator<float>(fs, "\t"));
	fs.close();
#endif		
	vector<float> smoothed_hist_vec(smoothed_hist, smoothed_hist + bins + 1);
	std::vector<float>::iterator biggest_it = std::max_element(smoothed_hist_vec.begin(), smoothed_hist_vec.end());
	size_t dis = std::distance(std::begin(smoothed_hist_vec), biggest_it);
	if (dis < 1)
	{
		return -1;
	}
	//gaussian half-width = 2.355*sigma, the top to half-top distance = 1.1775*sigma, set threshold to 2.58*sigma+top_index, this will cover about 99.73% background
	size_t half_index = 2 * dis;
	float dev = 50.0f, min_dev = 1000.0f, half_top = *biggest_it / 2.0f;
	for (size_t i = dis; i < end / 2; i++)
	{
		float diff = abs(smoothed_hist[i] - half_top);
		if (diff < dev)
		{
			half_index = i;
			break;
		}
		if (diff < min_dev)
		{
			min_dev = diff;
			half_index = i;
		}
	}
	index = 5000;
	for (size_t i = 2 * dis; i < end / 2; i++)
	{
		if (smoothed_hist[i + 1] > smoothed_hist[i] && smoothed_hist[i] < smoothed_hist[i - 1])
		{
			index = i;
			break;
		}
	}
	//thresh = (2.548*half_index - 1.548*dis) * step;
	float sigma_5_idx = (2.191*half_index - 1.191*dis)*step, trough_idx = index*step;
	thresh = sigma_5_idx < trough_idx ? sigma_5_idx : trough_idx;
	return 0;
}

// binary the ints from its background
// first: fake an image using dnb-ints
// second: threshold the image using OTSU-binary methold, OTSU only applies on 8-bit image
int DNBScorer::thresholdDNBInts(const std::vector<BlockDNBInfo>& blocksdnbinfo, float& thresh, cv::Mat& img_ints_32F, cv::Mat& binary_mask, vector<vector<Point2f>>& loc)
{
	assert(this->DNB_NUM_X.size() > 2 && this->DNB_NUM_Y.size() > 2);	
	unsigned int img_rows = std::accumulate(std::begin(this->DNB_NUM_Y), std::end(this->DNB_NUM_Y), -3 * this->DNB_NUM_Y.size());
	unsigned int img_cols = std::accumulate(std::begin(this->DNB_NUM_X), std::end(this->DNB_NUM_X), -3 * this->DNB_NUM_X.size());
	cv::Mat img_ints = cv::Mat::zeros(img_rows, img_cols, CV_32F);
	vector<float> ints(img_rows*img_cols);
#ifdef DEBUG
	//vector<Point2f> locs(img_rows*img_cols);
#endif
	loc.clear(); loc.resize(img_rows);
	size_t start_x = 0, start_y = 0, index = 0;
	for (size_t i = 0; i < this->BLOCKROW; i++)
	{
		for (size_t j = 0; j < this->BLOCKCOL; j++)
		{
			size_t block_id = i*this->BLOCKCOL + j;
			assert(blocksdnbinfo[block_id].dnbints.size() == (this->DNB_NUM_Y[i] - 3)*(this->DNB_NUM_X[j] - 3));
			for (size_t row = 0; row < this->DNB_NUM_Y[i] - 3; row++)
			{
				for (size_t col = 0; col < this->DNB_NUM_X[j] - 3; col++)
				{
					float dnb_int = blocksdnbinfo[block_id].dnbints[row*(this->DNB_NUM_X[j] - 3) + col].intensity;
					img_ints.at<float>(start_y + row, start_x + col) = dnb_int;
					ints[index] = dnb_int;
#ifdef DEBUG
					//locs[index] = blocksdnbinfo[block_id].dnbints[row*(this->DNB_NUM_X[j] - 3) + col].loc;
#endif					
					index++;
					loc[start_y + row].push_back(blocksdnbinfo[block_id].dnbints[row*(this->DNB_NUM_X[j] - 3) + col].loc);
				}
			}
			start_x += this->DNB_NUM_X[j] - 3;
		}
		start_y += this->DNB_NUM_Y[i] - 3;
		start_x = 0;
	}
	img_ints_32F = img_ints.clone();
#ifdef DEBUG
	/*std::ofstream fs("./loc.txt");
	std::copy(locs.begin(), locs.end(), std::ostream_iterator<Point2f>(fs, "\n"));
	fs.close();*/
#endif
	if (getBinaryThresh(ints, thresh) < 0)
	{
		return -1;
	}
	cv::threshold(img_ints, binary_mask, thresh, 1, cv::THRESH_BINARY);

#ifdef  DEBUG
	std::cout << cv::countNonZero(binary_mask) << std::endl;

#endif //  DEBUG	
	return 0;
}


/** @brief y=kx+b, use least-square formula to calculate k
k = (n*sum(x*y) - sum(x)*sum(y))/(n*sum(x^2) - sum(x)^2)

@param x_vec         vector record x value
@param y_vec         vector record y value
@param valid_index   only use data in valid_index set for fitting
@param R2            R_square = 1 - sum((y-y')^2) / sum((y-avg_y)^2)   used to evaluate the fit model
return k
*/
float DNBScorer::calK(const vector<float>& x_vec, const vector<float>& y_vec, const vector<size_t>& valid_index, float& R2)
{
	assert(x_vec.size() == y_vec.size() && x_vec.size() > 0);
	float sum_xy = 0.0f, sum_x2 = 0.0f, sum_y2 = 0.0f, sum_x = 0.0f, sum_y = 0.0f;
	for (vector<size_t>::const_iterator it = valid_index.cbegin(); it != valid_index.cend(); ++it)
	{
		float x = x_vec[*it], y = y_vec[*it];
		sum_xy += x * y;
		sum_x2 += x * x;
		sum_x += x;
		sum_y += y;
		sum_y2 += y * y;
	}

	int n = valid_index.size();
	float k = (n*sum_xy - sum_x * sum_y) / (n*sum_x2 - sum_x*sum_x);
	float b = (sum_y - k*sum_x) / n;
	float err = 0.0f, avg_y = sum_y / n, std_y = 0.0f;
	for (vector<size_t>::const_iterator it = valid_index.cbegin(); it != valid_index.cend(); ++it)
	{
		float x = x_vec[*it], y = y_vec[*it];
		float fy = x *k + b;
		err += (y - fy) * (y - fy);
		std_y += (y - avg_y) * (y - avg_y);
	}
	R2 = 1 - err / std_y;
	return k;
}


float DNBScorer::sratio_percent = 0.5f;
float DNBScorer::intensity_percent = 0.1f;
/** @brief Calculate energy spread ratio by dividing the DNB intensity of the sum intensity of its 8-neighbors.
Provided that its neigbors are all not emitted.

@param  img_ints_32F (32-bit floating point) recored the DNBs intensity.
@param  binary_mask({0,1} 8-bit) record DNB is emitted or not.
@param  loc                      location (type: Point2f) of emit DNB
@param  signal                   median value of emit DNBs intensity
@param  intscore                 spread_ratio and bf[2] and their cooresponding R2
*/
int DNBScorer::calEnergySpreadRatio_leastSquare(const cv::Mat& img_ints_32F, const cv::Mat& binary_mask, const vector<vector<Point2f>>& loc, float& signal, INTScore& intscore)
{
	vector<float> spread_ratio_vec, signal_vec, focus_signal_vec, sum_all_vec;
	typedef vector<float> vec_f;
	vec_f neighbors[2];
	float tmp = 0.0f;
	for (size_t y = 1; y < binary_mask.rows - 1; y++)
	{
		for (size_t x = 1; x < binary_mask.cols - 1; x++)
		{
			if (judgePeakPoint_binary(binary_mask, cv::Point(x, y), tmp))
			{
				float sum_4_neighbors = img_ints_32F.at<float>(y - 1, x) + img_ints_32F.at<float>(y, x - 1) + img_ints_32F.at<float>(y, x + 1) + img_ints_32F.at<float>(y + 1, x);
				float sum_4_diagonals = img_ints_32F.at<float>(y - 1, x - 1) + img_ints_32F.at<float>(y - 1, x + 1) + img_ints_32F.at<float>(y + 1, x - 1) + img_ints_32F.at<float>(y + 1, x + 1);
				neighbors[0].push_back(sum_4_neighbors);
				neighbors[1].push_back(sum_4_diagonals);
				float sum_all = sum_4_diagonals + sum_4_neighbors + img_ints_32F.at<float>(y, x);
				spread_ratio_vec.push_back(img_ints_32F.at<float>(y, x) / sum_all);
				focus_signal_vec.push_back(img_ints_32F.at<float>(y, x));
				sum_all_vec.push_back(sum_all);
				x += 1;
			}
		}
	}

	for (size_t y = 0; y < binary_mask.rows; y++)
	{
		for (size_t x = 0; x < binary_mask.cols; x++)
		{
			if (binary_mask.at<float>(y, x) == 1)
			{
				signal_vec.push_back(img_ints_32F.at<float>(y, x));
			}
		}
	}

	if (signal_vec.size() < 1)
	{
		return SIZE_ERROR;
	}

	vector<float> spread_ratio_vec_sorted = spread_ratio_vec;
	vector<float> focus_signal_vec_sorted = focus_signal_vec;
	
	float spread_ratio_th = getThreshold(spread_ratio_vec_sorted, DNBScorer::sratio_percent);
	float intensity_th = getThreshold(focus_signal_vec_sorted, DNBScorer::intensity_percent);

	vector<size_t> valid_index;
	for (size_t i = 0; i < spread_ratio_vec.size(); i++)
	{
		if (spread_ratio_vec[i] > spread_ratio_th && focus_signal_vec[i] > intensity_th)
		{
			valid_index.push_back(i);
		}
	}
	float bf[2] = { 0.0f }, R2[2] = { 0.0f };
	float spread_ratio = 0.0f, R2_sr = 0.0f;
	size_t index = 0;
	for (size_t i = 0; i < 2; i++)
	{
		bf[i] = calK(focus_signal_vec, neighbors[i], valid_index, R2[i]);
		bf[i] /= 4.0f;
	}
#ifdef  DEBUG_WRITE_LOC
	std::ofstream fs("./neighbors.txt", std::ios::app);
	if (fs)
	{
		for (size_t i = 0; i < 9; i++)
		{
			std::copy(std::begin(neighbors[i]), std::end(neighbors[i]), std::ostream_iterator<float>(fs, "\t"));
			fs << "\n";
		}

	}
	fs.close();
#endif //  DEBUG_WRITE_LOC
	spread_ratio = 1.0f - calK(sum_all_vec, focus_signal_vec, valid_index, R2_sr);
	intscore.sr_R2 = std::make_pair(spread_ratio, R2_sr);
	intscore.bf1_R2 = std::make_pair(bf[0], R2[0]);
	intscore.bf2_R2 = std::make_pair(bf[1], R2[1]);
	std::nth_element(std::begin(signal_vec), std::begin(signal_vec) + signal_vec.size() / 2, std::end(signal_vec));
	signal = signal_vec[signal_vec.size() / 2];
	return 0;
}

/** @brief Get DNB intensity related scores such as SNR, spread_ratio, bleeding_factor with trackcross.

@param img_fn        input image filename
@param intscore      output dnb intensity related scores
@param config_fn     intput  setting config filename
*/
int DNBScorer::getDNBScore(const string& img_fn, INTScore& intscore)
{
	try
	{
		vector<BlockInt> blocks;
		std::vector<BlockDNBInfo> blocksdnbinfo;
		cv::Mat& img = cv::imread(img_fn, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
		int ret = extractBlocksDnbs(img, blocks, blocksdnbinfo);
		if (ret < 0)
		{
			return ret;
		}
		float thresh = 0.0f;
		cv::Mat img_ints_32F, binary_mask;
		vector<vector<Point2f>> loc;
		ret = thresholdDNBInts(blocksdnbinfo, thresh, img_ints_32F, binary_mask, loc);
		if (ret < 0)
		{
			return ret;
		}
		float signal = 0.0f;
		ret = calEnergySpreadRatio_leastSquare(img_ints_32F, binary_mask, loc, signal, intscore);
		if (ret < 0)
		{
			return ret;
		}
		float snr = 0.0f;
		ret = getImgSNR(blocks, signal, snr);
		intscore.snr = snr;
		return ret;
	}
	catch (const std::exception&)
	{
		throw;
	}
	return -1;
}

//get image snr with trackline
int DNBScorer::getImgSNR(const vector<BlockInt>& blocks, const float& signal, float& snr)
{
	float bk_std = 0.0f;
	int ret = getBkStd(blocks, bk_std);
	if (ret < 0)
	{
		return ret;
	}
	if (bk_std < 1e-13)
	{
		snr = 0.0f;		
		return 0;
	}	
	snr = signal / bk_std;
	return 0;
}