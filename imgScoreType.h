#pragma once
#include <opencv/cv.h>

enum ERROR_CODE
{
	OTHERS = -1,
	WRONG_SOLVE = -2,
	ZERO_COUNT = -3,
	READ_CONFIG_ERROR = -4,
	FILE_NOT_EXIST = -5,
	FIND_TC_FAILED = -6,
	SIZE_ERROR = -7,
	INIT_PITCH_SIZE_ERROR = -8,
	DIVIDED_BY_ZERO = -9,
	INIT_BLOCK_ERROR = -10
};

template<typename T>
inline static bool judgePeakPoint_binary(const cv::Mat& img, const cv::Point& pt_in, T val)
{
	if (img.at<T>(pt_in.y, pt_in.x) <= img.at<T>(pt_in.y - 1, pt_in.x - 1)
		|| img.at<T>(pt_in.y, pt_in.x) <= img.at<T>(pt_in.y - 1, pt_in.x)
		|| img.at<T>(pt_in.y, pt_in.x) <= img.at<T>(pt_in.y - 1, pt_in.x + 1)
		|| img.at<T>(pt_in.y, pt_in.x) <= img.at<T>(pt_in.y, pt_in.x - 1)
		|| img.at<T>(pt_in.y, pt_in.x) <= img.at<T>(pt_in.y, pt_in.x + 1)
		|| img.at<T>(pt_in.y, pt_in.x) <= img.at<T>(pt_in.y + 1, pt_in.x - 1)
		|| img.at<T>(pt_in.y, pt_in.x) <= img.at<T>(pt_in.y + 1, pt_in.x)
		|| img.at<T>(pt_in.y, pt_in.x) <= img.at<T>(pt_in.y + 1, pt_in.x + 1))
	{
		return false;
	}
	else
	{
		//std::cout << pt_in << std::endl;
		return true;
	}
}
