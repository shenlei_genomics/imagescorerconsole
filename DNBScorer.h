#pragma once
#include <string>
#include <vector>
#include "imgScoreType.h"
#include "opencv\cv.h"
#include "BlockInt.h"
#include "Intensity.h"
#include "TrackDetector.h"

using std::string;
using std::vector;
/*@DNB intensity related score, includes signal,bf[2],spread_ratio and their cooresponding R2
bf[2]'s order is :  1   0   1
0   *   0
1   0   1
0.25 * sum of 4-neigbhors/center_spot_intensity = bf[0]
0.25 * sum of 4-diagnals/center_spot_intensity = bf[1]
use least-square fitting to calculate k in y=kx+b
@*/
typedef std::pair<float, float> Score;
struct INTScore
{
	float snr;
	Score sr_R2;
	Score bf1_R2;
	Score bf2_R2;
	INTScore() :snr(0.0f) {};
};

class DNBScorer
{	
public:
	DNBScorer() :is_extend(false), dividedRadius(0.6){};
	~DNBScorer();
	DNBScorer(const bool ext, const float r):is_extend(ext), dividedRadius(r){};

public:
	int init(const string& config_fn);
	int getDNBScore(const string& img_fn, INTScore& intscore);

private:
	vector<int> DNB_NUM_X;
	vector<int> DNB_NUM_Y;
	vector<int> dnb_numsX_tcdet, dnb_numsY_tcdet;
	bool is_extend;
	float dividedRadius;
	unsigned int unitNum;
	size_t BLOCKROW, BLOCKCOL;	
	
private:
	static float sratio_percent;
	static float intensity_percent;

private:
	int initBlockInts(const cv::Mat& img, const vector<Point2f> &trackcross, vector<BlockInt> &blocks);
	int extractBlocksDnbs(const cv::Mat& img, vector<BlockInt>& blocks, std::vector<BlockDNBInfo>& blocksdnbinfo);
	void fillVerts(const vector<float>& verbk, const size_t& row, const size_t& col, vector<vector<bool>>& tc_if_traverse, vector<float>& tcbk_vec);
	int getBkStd(const std::vector<BlockInt>& blocks, float& bk_std);
	int getBinaryThresh(const std::vector<float>& intens, float& thresh);
	int thresholdDNBInts(const std::vector<BlockDNBInfo>& blocksdnbinfo, float& thresh, cv::Mat& img_ints_32F, cv::Mat& binary_mask, vector<vector<Point2f>>& loc);
	float calK(const vector<float>& x_vec, const vector<float>& y_vec, const vector<size_t>& valid_index, float& R2);
	int calEnergySpreadRatio_leastSquare(const cv::Mat& img_ints_32F, const cv::Mat& binary_mask, const vector<vector<Point2f>>& loc, float& signal, INTScore& intscore);
	int getImgSNR(const vector<BlockInt>& blocks, const float& signal, float& snr);
};

