#include "calSigma.h"

int calRSqure(const cv::Mat& m, const cv::Mat& params, const cv::Mat& Y, float& R2, const unsigned int n = 9);

//---The algorithm is referred to <基于高斯拟合的激光光斑中心定位算法> ------------------------------//
int calSingleDNBSigma(const cv::Mat& img, const cv::Point& loc, float& sigma_x, float& sigma_y, float& R2, const unsigned int radius)
{
	// n: number of samples 
	const unsigned int n = (2 * radius + 1) * (2 * radius + 1);
	cv::Mat m = cv::Mat::ones(cv::Size(5, n), CV_32F);
	cv::Mat Y = cv::Mat::ones(cv::Size(1,n), CV_32F);
	size_t index = 0;
	for (size_t y = loc.y - radius; y < loc.y + radius+1; y++)
	{
		for (size_t x = loc.x - radius; x < loc.x + radius+1; x++)
		{
			m.at<float>(index, 0) = x*x;
			m.at<float>(index, 1) = y*y;
			m.at<float>(index, 2) = x;
			m.at<float>(index, 3) = y;
			Y.at<float>(index,0) = logf(img.at<unsigned short>(y,x)+1.0f); // Add 1 to avoid log(0) error
			//debug
			//std::cout << "image at : " << y << "," << x << ":" << img.at<unsigned short>(y, x) << std::endl;
			//
			index++;
		}
	}	
	cv::Mat params = m.inv(1)*Y; // using DECOMP_SVD for matrix invert
	//debug
	//std::cout << "m:" << m << std::endl;
	//std::cout << "Y:" << Y << std::endl;
	//std::cout << "X:" << params << std::endl;
	//
	float params_0_0 = params.at<float>(0, 0);
	float params_1_0 = params.at<float>(1, 0);
	if (params_0_0 > -1e-23 || params_1_0 > -1e-23 )
	{
		return WRONG_SOLVE;
	}
	sigma_x = sqrtf(-1.0f / params.at<float>(0, 0));
	sigma_y = sqrtf(-1.0f / params.at<float>(1, 0));
	R2 = FLT_MIN;
	return calRSqure(m, params, Y, R2);	
}

//-----------------R_square = 1 - sum((y-y')^2) / sum((y-avg_y)^2)---------------------------
int calRSqure(const cv::Mat& m,const cv::Mat& params, const cv::Mat& Y, float& R2 ,const unsigned int n)
{
	assert(m.size()==cv::Size(5,n) && params.size() == cv::Size(1,5));
	cv::Mat fit_Y = m*params;
	cv::Mat err_mat = fit_Y - Y;
	//std::cout << "fit_Y:" << fit_Y << std::endl;
	//std::cout << "err_mat:" << err_mat << std::endl;
	cv::Mat err_mat2 = err_mat.t() * err_mat;
	//std::cout << "err_mat2:" << err_mat2 << std::endl;
	float err = err_mat2.at<float>(0, 0);
	float avg_Y = cv::sum(Y)(0)/(Y.rows*Y.cols + 0.0f);
	float tmp = 0.0f;
	for (size_t i = 0; i < Y.rows; i++)
	{
		tmp += (Y.at<float>(i, 0) - avg_Y) * (Y.at<float>(i, 0) - avg_Y);
	}
	if (tmp < 1e-023)
	{
		return DIVIDED_BY_ZERO;
	}
	R2 = 1 - err / tmp;
	return 0;
}

int calImgSigma(const cv::Mat& img, const std::vector<PeakInfo>& peak, std::vector<SigmaInfo>& sigma_vec, const float R2th_percent)
{
	using std::vector;
	vector<float> R2_vec;
	vector<SigmaInfo> tmp_sigma_vec;
	for (vector<PeakInfo>::const_iterator it = peak.begin(); it != peak.end(); ++it)
	{
		float sigma_x = 0.0f, sigma_y = 0.0f, R2=0.0f;
		if (calSingleDNBSigma(img, it->loc, sigma_x, sigma_y, R2) > -1)
		{
			tmp_sigma_vec.push_back(SigmaInfo(it->loc, sigma_x, sigma_y, R2));
			R2_vec.push_back(R2);
		}
	}
	if (R2_vec.size() < 1)
	{
		return ZERO_COUNT;
	}
	float R2_th = getThreshold(R2_vec, R2th_percent);
	sigma_vec.clear();
	for (vector<SigmaInfo>::const_iterator it = tmp_sigma_vec.begin(); it != tmp_sigma_vec.end(); ++it)
	{
		if (it->R2 > R2_th)
		{
			sigma_vec.push_back(*it);
		}
	}
	return 0;
}

int testAdd(int a, int b)
{
	return a + b;
}

int getFocusScoreAndSigma(const char* img_fn,float& avg_focus_score, float& avg_sigma_x, float& avg_sigma_y, float& avg_R2, const bool ifsave_goodpoints)
{
	try
	{
		if (_access(img_fn, 0) < 0)
		{
			return -2;
		}
		cv::Mat img = cv::imread(img_fn, CV_LOAD_IMAGE_ANYDEPTH);
		cv::Mat img_tmp;
		//img(cv::Range(700,1200),cv::Range(700,1200)).copyTo(img_tmp);
		img.copyTo(img_tmp);
		img_tmp.convertTo(img_tmp, CV_32FC1);
		std::vector<PeakInfo> peaks;
		findPeakPtsVals(img_tmp, peaks);
		if (ifsave_goodpoints)
		{
			char drive[256], dir[256], fname[256], ext[256];
			_splitpath(img_fn, drive, dir, fname, ext);
			std::string goodpoints_fn = std::string(fname) + "_gp";
			char gp_fullname[256];
			_makepath(gp_fullname, drive, dir, goodpoints_fn.c_str(), ".txt");
			std::ofstream fs(gp_fullname);
			if (fs)
			{
				for (std::vector<PeakInfo>::const_iterator it = peaks.begin(); it != peaks.end(); ++it)
				{
					fs << it->loc.x << "\t" << it->loc.y << "\n";
				}
			}
			else
			{				
				return -3;
			}
			fs.close();
			
		}
		
		avg_focus_score = 0.0f;
		for (std::vector<PeakInfo>::const_iterator it = peaks.begin(); it != peaks.end(); ++it)
		{
			avg_focus_score += it->focus;
		}
		if (peaks.size() > 0)
		{
			avg_focus_score /= peaks.size();
		}
		else
		{
			return -1;
		}
		std::vector<SigmaInfo> sigma_vec;
		if (calImgSigma(img, peaks, sigma_vec) < 0)
			return -2;
		avg_sigma_x = 0.0f; avg_sigma_y = 0.0f, avg_R2 = 0.0f;
		for (std::vector<SigmaInfo>::const_iterator it = sigma_vec.begin(); it != sigma_vec.end(); ++it)
		{
			avg_sigma_x += it->sigma_x;
			avg_sigma_y += it->sigma_y;
			avg_R2 += it->R2;
		}
		avg_sigma_x /= sigma_vec.size();
		avg_sigma_y /= sigma_vec.size();
		avg_R2 /= sigma_vec.size();
		//std::cout << "Succeed!" << std::endl;
		return 0;
	}
	catch (const std::exception&)
	{
		throw;
	}
	return -1;	
}

int getFocusScoreAndSigma(const cv::Mat& img, float& avg_sigma_x, float& avg_sigma_y, float& avg_R2)
{
	try
	{
		if (img.empty())
		{
			return -2;
		}
		//cv::Mat img = cv::imread(img_fn, CV_LOAD_IMAGE_ANYDEPTH);
		cv::Mat img_tmp;
		//img(cv::Range(700,1200),cv::Range(700,1200)).copyTo(img_tmp);
		img.copyTo(img_tmp);
		img_tmp.convertTo(img_tmp, CV_32FC1);
		std::vector<PeakInfo> peaks;
		findPeakPtsVals(img_tmp, peaks);
		
		if (peaks.size() < 1)
		{
			return -1;
		}
		
		std::vector<SigmaInfo> sigma_vec;
		if (calImgSigma(img, peaks, sigma_vec) < 0)
			return -2;
		avg_sigma_x = 0.0f; avg_sigma_y = 0.0f, avg_R2 = 0.0f;
		for (std::vector<SigmaInfo>::const_iterator it = sigma_vec.begin(); it != sigma_vec.end(); ++it)
		{
			avg_sigma_x += it->sigma_x;
			avg_sigma_y += it->sigma_y;
			avg_R2 += it->R2;
		}
		avg_sigma_x /= sigma_vec.size();
		avg_sigma_y /= sigma_vec.size();
		avg_R2 /= sigma_vec.size();
		//std::cout << "Succeed!" << std::endl;
		return 0;
	}
	catch (const std::exception&)
	{
		throw;
	}
	return -1;
}


