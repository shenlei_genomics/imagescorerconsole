#pragma once
#include "calSigma.h"
#include "DNBScorer.h"
#include <fstream>

int getFiles(const std::string& path, const std::string& ext, std::vector<std::string>& files)
{
	intptr_t file_handle = 0;
	struct _finddata_t file_info;
	std::string path_name, ext_name;
	if (strcmp(ext.c_str(), "") != 0)//if not empty
		ext_name = "\\*" + ext;
	else
		ext_name = "\\*";

	if ((file_handle = _findfirst(path_name.assign(path).append(ext_name).c_str(), &file_info)) != -1)
	{
		do
		{
			if (file_info.attrib & _A_SUBDIR)
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					getFiles(path_name.assign(path).append("\\").append(file_info.name), ext, files);
				}
			}
			else
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					files.push_back(path_name.assign(path).append("\\").append(file_info.name));
					//files.push_back(file_info.name);
				}
			}
		} while (_findnext(file_handle, &file_info) == 0);
		_findclose(file_handle);
	}
	return 0;
}

static void help(char* progName)
{
	std::cout << std::endl
		<< "This program output image's scores: avg_focus_score  sigma_x sigma_y sigma_R2 SNR energy_spread_ratio esr_R2 bleeding_factor1 bf1_R2 bleeding_factor2 bf2" << std::endl
		<< "Usage:" << std::endl
		<< progName << " imgs_directory setting_file_path result_csv_path" << std::endl
		<< "For example:" << std::endl
		<< progName << " E:/Lane1/Cycle1  ./settings.config  ./score.csv" << std::endl;

	//<< progName << " imgsPath outputTxtFn [-s]"                       << endl
	//<< "-s is optional, if input -s, will print all of the power spectrum info.";
}

int genCornerImgs(Mat& I, const int& rows, const int& cols, vector<Mat>& corners)
{
	using cv::Point;
	using cv::Range;
	if (I.empty() || I.rows < rows || I.cols < cols)
	{
		return -1;
	}	
	Mat tl = I(Range(0, rows), Range(0, cols));
	Mat tr = I(Range(0, rows), Range(I.cols - cols, I.cols));
	Mat dl = I(Range(I.rows - rows, I.rows), Range(0, cols));
	Mat dr = I(Range(I.rows - rows, I.rows), Range(I.cols - cols, I.cols));
	Point start = Point((I.cols >> 1) - (cols >> 1), (I.rows >> 1) - (rows >> 1));
	Mat center = I(Range(start.y, start.y + rows), Range(start.x, start.x + cols));
	corners.clear();
	corners.push_back(tl);
	corners.push_back(tr);
	corners.push_back(dr);
	corners.push_back(dl);
	corners.push_back(center);
	//imwrite("./center.tif", center);
}

int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		help(argv[0]);
		return -1;
	}
	const string img_dir = argv[1];
	const string settings_fn = argv[2];
	const string result_fn = argv[3];
	
	if (_access(img_dir.c_str(), 0) < 0 || _access(settings_fn.c_str(), 0) < 0)
	{
		std::cout << "Input path or file not exist!" << std::endl;
		return FILE_NOT_EXIST;
	}
	vector<string> files;
	if (getFiles(img_dir, "*.tif", files) < 0)
	{
		std::cout << "get files in directory " <<img_dir << " error!" << std::endl;
		return FILE_NOT_EXIST;
	}
	DNBScorer dnbscorer;
	if (dnbscorer.init(settings_fn) < 0)
	{
		std::cout << "Init DNBScorer failed! " << "\tIn:" << __FUNCTION__ << "\t" << __LINE__<< std::endl;
		return -1;
	}

	std::ofstream fs(result_fn, std::ios::app);
	if (!fs)
	{
		std::cout << "Trying to write file:" << result_fn << " failed!" << std::endl;
		return -1;
	}
	fs << "tif_filename,avg_focus_score,sigma_x,sigma_y,sigma_R2,tl_sigma_x,tl_sigma_y,tl_sigma_R2,tr_sigma_x,tr_sigma_y,tr_sigma_R2,br_sigma_x,br_sigma_y,br_sigma_R2,bl_sigma_x,bl_sigma_y,bl_sigma_R2,ct_sigma_x,ct_sigma_y,ct_sigma_R2,SNR,energy_spread_ratio,esr_R2,bleeding_factor1,bf1_R2,bleeding_factor2,bf2\n";

	for (vector<string>::const_iterator it = files.cbegin(); it != files.cend(); ++it)
	{
		std::cout << "Processing img: " << *it << std::endl;
		fs << *it << ",";
		float score[26] = {0.0f};
		cv::Mat img = cv::imread(it->c_str(), CV_LOAD_IMAGE_ANYDEPTH);
		if (img.empty())
		{
			std::copy(score, score + 26, std::ostream_iterator<float>(fs, ","));
			fs << "\n";
			continue;
		}
		getFocusScoreAndSigma(it->c_str(), score[0], score[1], score[2], score[3], false);		
		vector<Mat> corners;
		if (genCornerImgs(img, img.rows/3, img.cols/3, corners) < 0 || corners.size() != 5)
			return -1;
		int index = 4;
		for (vector<Mat>::const_iterator it = corners.cbegin(); it != corners.cend(); ++it)
		{
			getFocusScoreAndSigma(*it, score[index], score[index + 1], score[index + 2]);
			index += 3;
		}
		INTScore intscore;
		if (dnbscorer.getDNBScore(*it, intscore) == 0)
		{
			score[19] = intscore.snr;
			score[20] = intscore.sr_R2.first;
			score[21] = intscore.sr_R2.second;
			score[22] = intscore.bf1_R2.first;
			score[23] = intscore.bf1_R2.second;
			score[24] = intscore.bf2_R2.first;
			score[25] = intscore.bf2_R2.second;
		}
		std::copy(score, score + 26, std::ostream_iterator<float>(fs, ","));
		fs << "\n";
	}
	fs.close();
	return 0;
}